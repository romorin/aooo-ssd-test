### DO NOT USE ####


CXX=g++
CXXFLAGS = -Wall -g

BIN_PATH = bin
OBJ_PATH = obj
TEST_PATH = test

INC = ''

#src = $(wildcard *.cpp)
TEST_SRC = $(wildcard test/*.cpp)
TEST_OBJECTS = $(TEST_SRC:%.cpp=obj/%.o)
TEST_OBJECTS_DIRS:=$(dir $(TEST_OBJECTS))

build:
	platformio run

upload:
	platformio run -t upload

# $(INC)

$(TEST_OBJECTS): $(TEST_SRC)
	$(CXX) -c -o $@ $< $(CXXFLAGS)

test: $(TEST_OBJECTS_DIRS) $(TEST_OBJECTS)
	$(CXX) -o $(BIN_PATH)/$@ $(filter %.o, $^) $(LDFLAGS)

.PHONY: clean

$(TEST_OBJECTS_DIRS):
	mkdir -p $@

clean:
	rm bin/*
	rm -r obj/*
