#ifndef OBSERVER_H
#define OBSERVER_H

template <typename EVENT>
class Observer
{
public:
	virtual ~Observer() {}
	virtual void update(const EVENT& event) = 0;
};

#endif
