#include "CharSelectionCenter.h"

#include <cstring>

CharSelectionCenter::CharSelectionCenter()
{}

CharSelectionCenter::~CharSelectionCenter()
{}

char CharSelectionCenter::getSegments(
	const size_t position,
	const size_t availableDigits,
	const SsdContent& content) const
{
	size_t len = content.length();
	size_t pad = (availableDigits - len) / 2;

	if (position < pad || position >= len + pad)
	{
		return content.getSegments(' ', false);
	}
	else
	{
		return content.getSegments(position-pad);
	}
}
