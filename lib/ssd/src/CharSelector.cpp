#include "CharSelector.h"

#include <cstring>

CharSelector::CharSelector(
		SsdContent& content,
		const CharSelectionStrategy& centerSelectionStrategy,
		const CharSelectionStrategy& leftSelectionStrategy,
		const CharSelectionStrategy& rightSelectionStrategy,
		const CharSelectionStrategy& beginningOverflowStrategy,
		const CharSelectionStrategy& errorOverflowStrategy,
		const CharSelectionStrategy& scrollOverflowStrategy) :
	_content(content),
	_centerSelectionStrategy(centerSelectionStrategy),
	_leftSelectionStrategy(leftSelectionStrategy),
	_rightSelectionStrategy(rightSelectionStrategy),
	_currentPositioningStrategy(&_leftSelectionStrategy),
	_beginningOverflowStrategy(beginningOverflowStrategy),
	_errorOverflowStrategy(errorOverflowStrategy),
	_scrollOverflowStrategy(scrollOverflowStrategy),
	_currentOverflowStrategy(&beginningOverflowStrategy)
{
	// @todo assert
}

void CharSelector::writeString(const char* const entry)
{
	_content.writeString(entry);
}


void CharSelector::setPositioningStrategy(
	const PositioningStrategy strategy)
{
	switch (strategy) {
		case LEFT:
			_currentPositioningStrategy = &_leftSelectionStrategy;
			break;
		case RIGHT:
			_currentPositioningStrategy = &_rightSelectionStrategy;
			break;
		case CENTER:
			_currentPositioningStrategy = &_centerSelectionStrategy;
			break;
	}
}

void CharSelector::setOverflowStrategy(
	const OverflowStrategy strategy)
{
	switch (strategy) {
		case BEGINNING:
			_currentOverflowStrategy = &_beginningOverflowStrategy;
			break;
		case ERROR:
			_currentOverflowStrategy = &_errorOverflowStrategy;
			break;
		case SCROLL:
			_currentOverflowStrategy = &_scrollOverflowStrategy;
			break;
	}
}

unsigned char CharSelector::getSegments(
	const size_t position, const size_t availableDigits) const
{
	if (_content.length() > availableDigits)
	{
		return _currentOverflowStrategy->getSegments(position, availableDigits, _content);
	}
	else
	{
		return _currentPositioningStrategy->getSegments(position, availableDigits, _content);
	}
}
