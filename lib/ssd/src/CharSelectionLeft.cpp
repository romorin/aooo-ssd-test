#include "CharSelectionLeft.h"

#include <cstring>

CharSelectionLeft::CharSelectionLeft()
{}

CharSelectionLeft::~CharSelectionLeft()
{}

char CharSelectionLeft::getSegments(
	const size_t position,
	const size_t availableDigits,
	const SsdContent& content) const
{
	if (position >= content.length() || position >= availableDigits)
	{
		return content.getSegments(' ', false);
	}
	else
	{
		return content.getSegments(position);
	}
}
