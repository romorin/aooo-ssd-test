#include "SsdControllerImpl.h"

SsdControllerImpl::SsdControllerImpl(SevenSD& sevenSD, CharSelector& selector) :
	_sevenSD(sevenSD),
	_selector(selector)
{}

size_t SsdControllerImpl::availableDigits() const
{
	return _sevenSD.availableDigits();
}

void SsdControllerImpl::writeString(const char* const entry)
{
	_selector.writeString(entry);
}

void SsdControllerImpl::setPositioningStrategy(
	const PositioningStrategy strategy)
{
	_selector.setPositioningStrategy(strategy);
}

void SsdControllerImpl::setOverflowStrategy(
	const OverflowStrategy strategy)
{
	_selector.setOverflowStrategy(strategy);
}
