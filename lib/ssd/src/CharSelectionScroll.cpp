#include "CharSelectionScroll.h"

#include <cstring>

CharSelectionScroll::CharSelectionScroll(const unsigned long scrollMillis,
	const unsigned long edgePauseMillis, Subject<LoopEvent>& subject) :
	_scrollMillis(scrollMillis), _edgePauseMillis(edgePauseMillis), _setTimeMillis(0),
	_lastLoopMillis(0), _timeSet(false)
{
	subject.attach(*this);
}

CharSelectionScroll::~CharSelectionScroll()
{}

char CharSelectionScroll::getSegments(
	const size_t position,
	const size_t availableDigits,
	const SsdContent& content) const
{
	size_t len = content.length();
	// out of bounds
	if (position > availableDigits || position >= len)
	{
		return content.getSegments(' ', false);
	}

	int missingDigits = len - availableDigits;

	// fallback to left positioning if all chars can fit on the screen
	if (missingDigits <= 0)
	{
		return content.getSegments(position);
	}

	unsigned long beforeLastCharPeriod =
		_edgePauseMillis + (missingDigits - 1) * _scrollMillis;
	unsigned long totalPeriod = beforeLastCharPeriod + _edgePauseMillis;
	unsigned long offsetMillis = (_lastLoopMillis - _setTimeMillis) % totalPeriod;

	size_t currentOffset = 0;

	if (offsetMillis < _edgePauseMillis)
	{
		currentOffset = 0;
	}
	else if (offsetMillis > beforeLastCharPeriod)
	{
		currentOffset = missingDigits;
	}
	else
	{
		currentOffset = 1 + (offsetMillis - _edgePauseMillis) / _scrollMillis;
	}

	return content.getSegments(currentOffset + position);
}

void CharSelectionScroll::update(const LoopEvent& event)
{
	if(!_timeSet)
	{
		_timeSet = true;
		_setTimeMillis = event.millis;
	}
	_lastLoopMillis = event.millis;
}
