#ifndef CHAR_SELECTION_STUB_H
#define CHAR_SELECTION_STUB_H

#include "CharSelectionStrategy.h"
#include "SsdContent.h"

#include "stddef.h"

class CharSelectionStub : public CharSelectionStrategy
{
public:
	CharSelectionStub(char charToReturn) : _charToReturn(charToReturn) {}
	virtual ~CharSelectionStub() {}

	virtual char getSegments(
		const size_t position,
		const size_t availableDigits,
		const SsdContent& content) const
	{
		return _charToReturn;
	}

	char _charToReturn;
};

#endif
