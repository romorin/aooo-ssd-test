#ifndef SSD_STUB_H
#define SSD_STUB_H

#include "SevenSD.h"

#include <array>

#include "stddef.h"

template <size_t NUM_DIGITS>
class SsdStub : public SevenSD
{
public:
	SsdStub() {}
	virtual ~SsdStub() {}

	virtual size_t availableDigits() const
	{
		return NUM_DIGITS;
	}
	virtual void write(const char digits, const size_t position)
	{
		_written[position] = digits;
	}

	std::array<char, NUM_DIGITS> _written;
};

#endif
