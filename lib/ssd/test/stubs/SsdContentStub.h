#ifndef SSD_CONTENT_STUB_H
#define SSD_CONTENT_STUB_H

#include "SsdContent.h"

#include <string>

#include "stddef.h"

class SsdContentStub : public SsdContent
{
public:
	std::string content;

	SsdContentStub() {}

	virtual ~SsdContentStub() {}

	virtual void writeString(const char* const entry)
	{
		content = entry;
	}

	size_t len = 2;
	virtual size_t length() const
	{
		return len;
	}

	mutable char segment = 0;
	mutable size_t position = 0;

	virtual unsigned char getSegments(
		size_t position) const
	{
		this->position = position;
		return segment;
	}

	mutable char value;
	mutable bool withDot;

	virtual unsigned char getSegments(const char value, const bool withDot) const
	{
		this->value = value;
		this->withDot = withDot;
		return segment;
	}
};

#endif
