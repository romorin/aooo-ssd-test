#include "catch2/catch.hpp"

#include "CharSelector.h"
#include "SsdContent.h"
#include "SegmentsDict.h"

#include "stubs/CharSelectionStub.h"
#include "stubs/SsdContentStub.h"

#include <string>

SCENARIO( "CharSelector works" ) {
	GIVEN( "A char selector and some stuff" ) {
		SsdContentStub content;

		CharSelectionStub centerCharSelection('c');
		CharSelectionStub leftCharSelection('l');
		CharSelectionStub rightCharSelection('r');
		CharSelectionStub beginCharSelection('b');
		CharSelectionStub errorCharSelection('e');
		CharSelectionStub scrollCharSelection('s');

		CharSelector selector(content,
			centerCharSelection, leftCharSelection, rightCharSelection,
			beginCharSelection, errorCharSelection, scrollCharSelection);

		WHEN( "a short text is written" ) {
			selector.writeString("Hi");
			content.len = 2;

			THEN ( "should use centerCharSelection when selected" ) {
				selector.setPositioningStrategy(CENTER);
				REQUIRE(
					selector.getSegments(0, 3) == 'c'
				);
			}
			AND_THEN ( "should use leftCharSelection when selected" ) {
				selector.setPositioningStrategy(LEFT);
				REQUIRE(
					selector.getSegments(0, 3)  == 'l'
				);
			}
			AND_THEN ( "should use rightCharSelection when selected" ) {
				selector.setPositioningStrategy(RIGHT);
				REQUIRE(
					selector.getSegments(0, 3)  == 'r'
				);
			}
			AND_THEN ( "the text should be written" ) {
				REQUIRE(
					content.content  == "Hi"
				);
			}
		}

		AND_WHEN( "a long text is written" ) {
			selector.writeString("Good Bye");
			content.len = 8;

			THEN ( "should use centerCharSelection when selected" ) {
				selector.setOverflowStrategy(BEGINNING);
				REQUIRE(
					selector.getSegments(0, 3)  == 'b'
				);
			}
			AND_THEN ( "should use leftCharSelection when selected" ) {
				selector.setOverflowStrategy(ERROR);
				REQUIRE(
					selector.getSegments(0, 3)  == 'e'
				);
			}
			AND_THEN ( "should use rightCharSelection when selected" ) {
				selector.setOverflowStrategy(SCROLL);
				REQUIRE(
					selector.getSegments(0, 3)  == 's'
				);
			}
		}
	}
}
