#ifndef CHAR_SELECTION_SCROLL_H
#define CHAR_SELECTION_SCROLL_H

#include "include/Observer.h"
#include "include/Subject.h"

#include "LoopEvent.h"

#include "CharSelectionStrategy.h"
#include "SsdContent.h"

#include "stddef.h"

class CharSelectionScroll : public CharSelectionStrategy, public Observer<LoopEvent>
{
public:
	CharSelectionScroll(const unsigned long scrollMillis,
		const unsigned long endPauseMillis, Subject<LoopEvent>& subject);
	virtual ~CharSelectionScroll();

	virtual char getSegments(
		const size_t position,
		const size_t availableDigits,
		const SsdContent& content) const;

	virtual void update(const LoopEvent& event);

private:
	unsigned long _scrollMillis;
	unsigned long _edgePauseMillis;
	unsigned long _setTimeMillis;
	unsigned long _lastLoopMillis;

	bool _timeSet;
};

#endif
