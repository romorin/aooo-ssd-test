#ifndef SSD_CONTROLLER_IMPL_H
#define SSD_CONTROLLER_IMPL_H

#include "CharSelector.h"
#include "SevenSD.h"
#include "SsdController.h"

#include "stddef.h"

class SsdControllerImpl : public SsdController
{
public:
	SsdControllerImpl(
		SevenSD& sevenSD,
		CharSelector& selector);

	virtual size_t availableDigits() const;

	virtual void writeString(const char* const entry);

	virtual void setPositioningStrategy(
		const PositioningStrategy strategy);
	virtual void setOverflowStrategy(
		const OverflowStrategy strategy);

private:
	SevenSD& _sevenSD;
	CharSelector& _selector;
};

#endif
