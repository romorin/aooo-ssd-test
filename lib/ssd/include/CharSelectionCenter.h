#ifndef CHAR_SELECTION_CENTER_H
#define CHAR_SELECTION_CENTER_H

#include "CharSelectionStrategy.h"
#include "SsdContent.h"

#include "stddef.h"

class CharSelectionCenter : public CharSelectionStrategy
{
public:
	CharSelectionCenter();
	virtual ~CharSelectionCenter();

	virtual char getSegments(
		const size_t position,
		const size_t availableDigits,
		const SsdContent& content) const;
};

#endif
