#ifndef CHAR_SELECTION_STRATEGY_H
#define CHAR_SELECTION_STRATEGY_H

#include "SsdContent.h"

#include "stddef.h"

class CharSelectionStrategy
{
public:
	virtual ~CharSelectionStrategy() {}

	virtual char getSegments(
		const size_t position,
		const size_t availableDigits,
		const SsdContent& content) const = 0;
};

#endif
