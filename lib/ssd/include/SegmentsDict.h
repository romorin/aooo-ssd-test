#ifndef SEGMENTS_DICT_H
#define SEGMENTS_DICT_H

#include <array>

#define NUM_SEGMENTS_DICT_ENTRIES 38

class SegmentsDict {
public:
	SegmentsDict();

	const unsigned char getSegments(const char value, const bool withDot) const;

private:
};

#endif
