#ifndef SSD_CONTROLLER_H
#define SSD_CONTROLLER_H

#include "CharStrategies.h"

#include "stddef.h"

class SsdController
{
public:
	virtual size_t availableDigits() const = 0;

	virtual void writeString(const char* const entry) = 0;

	virtual void setPositioningStrategy(
		const PositioningStrategy strategy) = 0;
	virtual void setOverflowStrategy(
		const OverflowStrategy strategy) = 0;
};

#endif
