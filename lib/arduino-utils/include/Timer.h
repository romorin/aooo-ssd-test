#ifndef TIMER_H
#define TIMER_H

#include <functional>

class Timer
{
public:
	virtual ~Timer() {}
	virtual bool attach_ms(const unsigned long milliseconds, std::function<void ()> callback) = 0;
	virtual bool attach_us(const unsigned long microseconds, std::function<void ()> callback) = 0;

	virtual bool once_ms(const unsigned long milliseconds, std::function<void ()> callback) = 0;
	virtual bool once_us(const unsigned long microseconds, std::function<void ()> callback) = 0;
};

#endif
