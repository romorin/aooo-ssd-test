#ifndef PINS_H
#define PINS_H

template <typename HOLDER>
class Pins {
public:
	virtual ~Pins() {}
	virtual void write(const HOLDER data) = 0;
};

#endif
