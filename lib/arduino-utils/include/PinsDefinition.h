#ifndef PINS_DEFINITION_H
#define PINS_DEFINITION_H

#include <array>

template <size_t N>
struct PinsDefinition
{
	const std::array<char, N> id;
	const bool activeHigh;
};

#endif
