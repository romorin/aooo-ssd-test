#ifndef PINS_STUB_H
#define PINS_STUB_H

#include "Pins.h"

class PinsStub : public Pins<char>
{
public:
	PinsStub() {}
	virtual ~PinsStub() {}

	virtual void write(const char data) { _data = data; }

	char get() { return _data; }

private:
	char _data;
};

#endif
