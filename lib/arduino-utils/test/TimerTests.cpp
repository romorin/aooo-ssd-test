#include "catch2/catch.hpp"

#include "include/SubjectImpl.h"
#include "LoopEvent.h"
#include "TimerImpl.h"

SCENARIO( "Basic stuff works" ) {
	WHEN( "the file is run" ) {
		THEN( "Truth exists" ) {
			REQUIRE( true == true );
		}
	}
	WHEN( "we create a timer" ) {
		SubjectImpl<LoopEvent> subject;
		TimerImpl timer = TimerImpl(subject);

		THEN( "Truth still exists" ) {
			REQUIRE( true == true );
		}
	}
}

SCENARIO( "Timer rings" ) {
	GIVEN( "A timer and a callback" ) {
		SubjectImpl<LoopEvent> subject;
		TimerImpl timer = TimerImpl(subject);
		LoopEvent event = {0,0};

		unsigned long period = 10;

		int run = 0;
		auto callback = [&]() {
			++run;
		};

		WHEN( "A milli callback is attached" ) {
			timer.attach_ms(period,callback);

			THEN( "An update with a time less than the period should not trigger \
				the callback" )
			{
				timer.update({5, 5*1000});
				REQUIRE( run == 0 );
			}

			AND_THEN( "An update with a time larger than the period should \
				trigger the callback" )
			{
				timer.update({10, 10*1000});
				REQUIRE( run == 1 );
			}

			AND_THEN( "A second update with 1.5* the period should not trigger \
				a second time the callback" )
			{
				timer.update({10, 10*1000});
				timer.update({15, 15*1000});
				REQUIRE( run == 1 );
			}
		}

		AND_WHEN( "A micro callback is attached" ) {
			timer.attach_us(period,callback);

			THEN( "An update with a time larger than the period should \
				trigger the callback" )
			{
				timer.update({0, 10});
				REQUIRE( run == 1 );
			}
		}

		AND_WHEN( "A milli single callback is attached" ) {
			timer.once_ms(period,callback);

			THEN( "An update with a time larger than the period should \
				trigger the callback" )
			{
				timer.update({10, 0});
				REQUIRE( run == 1 );
			}

			AND_THEN( "A second update should not trigger \
				a second time the callback" )
			{
				timer.update({10, 0});
				timer.update({20, 0});
				REQUIRE( run == 1 );
			}
		}
	}
}
