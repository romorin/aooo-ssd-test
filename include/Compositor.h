#ifndef COMPOSITOR_H
#define COMPOSITOR_H

#include "include/SubjectImpl.h"

#include "ShiftedCounterBuilder.h"
#include "LoopEvent.h"
#include "PinsDefinition.h"

#include "SsdControllerBuilder.h"

#include "Arduino.h"

template <
	class TSSD = MultiplexedSSD,
	size_t PERIOD_US = 2000,
	size_t SCROLL_MILLIS = 500,
	size_t EDGE_SCROLL_MILLIS = 1000,
	size_t NUM_SEGMENTS = 8,
	size_t NUM_DISPLAYS = 4,
	size_t MAX_CONTENT_LENGTH = 120
>
class Compositor
{
public:
	Compositor(
		PinsDefinition<NUM_SEGMENTS> segmentPinsId,
		PinsDefinition<1> serialDataPinId,
		PinsDefinition<1> inputClockPinId,
		PinsDefinition<1> outputClockPinId) :
		_loopSubject(),
		_counterBuilder(_loopSubject, serialDataPinId,
			inputClockPinId, outputClockPinId),
		_controllerBuilder(segmentPinsId, _loopSubject, _counterBuilder.get())
	{}

	SsdController& get()
	{
		return _controllerBuilder.get();
	}

	void loop()
	{
		static LoopEvent event;
		event.millis = millis();
		event.micros = micros();
		_loopSubject.notify(event);
	}

private:
	SubjectImpl<LoopEvent> _loopSubject;
	ShiftedCounterBuilder <NUM_DISPLAYS, PERIOD_US> _counterBuilder;
	SsdControllerBuilder <TSSD, PERIOD_US, SCROLL_MILLIS, EDGE_SCROLL_MILLIS,
		NUM_SEGMENTS, NUM_DISPLAYS, MAX_CONTENT_LENGTH> _controllerBuilder;
};

#endif
