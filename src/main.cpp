/**

 */
#include "Arduino.h"
#include "Compositor.h"
#include "SsdController.h"
#include "CharStrategies.h"

Compositor<> compositor(
	{{D0,D1,D2,D3,D4,D5,D6,D10}, false},
	{{D7}, true},
	{{D8}, true},
	{{D9}, true}
);

void setup()
{
	compositor.get().setOverflowStrategy(SCROLL);
	compositor.get().writeString("Hello. world.");
}

void loop()
{
	compositor.loop();
}
