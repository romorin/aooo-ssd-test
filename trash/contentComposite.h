#ifndef CONTENT_COMPOSITE_H
#define CONTENT_COMPOSITE_H

#include "ssd/segContent.h"

#include <forward_list>

class ContentComposite : public SegContent {
public:
	struct Inner {
		const SegContent& content;
		const unsigned int availableDigits;
	};

	ContentComposite(std::forward_list<Inner> innerContent);
	virtual ~ContentComposite();

	virtual char getSegments(
		const unsigned int position, const unsigned int availableDigits) const;

private:
	std::forward_list<Inner> _innerContent;
};

#endif
