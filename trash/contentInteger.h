#ifndef CONTENT_INTEGER_H
#define CONTENT_INTEGER_H

#include "ssd/segContent.h"

#include <string>

class ContentInteger : public Segcontent {
public:
	ContentInteger();
	virtual ~ContentInteger();

	void setContent(const long number):
	virtual char getSegments(
		const unsigned int position, const unsigned int availableDigits) const;

private:
	std::string _contentStr;
};

#endif
