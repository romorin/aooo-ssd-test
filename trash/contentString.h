#ifndef CONTENT_STRING_H
#define CONTENT_STRING_H

#include "ssd/segContent.h"

#include <string>

class ContentString : public Segcontent {
public:
	ContentString();
	virtual ~ContentString();

	void setContent(std::string content):
	virtual char getSegments(
		const unsigned int position, const unsigned int availableDigits) const;

private:
	std::string _content;
};

#endif
