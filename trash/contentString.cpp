#include "ssd/contentString.h"

ContentString::ContentString() :
	_content("")
{
}

void ContentString::setContent(std::string content)
{
	_content = content;
}

char ContentString::getSegments( const unsigned int position,
	const unsigned int availableDigits) const
{
	if (position >= availableDigits)
	{
		return '';
	}

	if (position > _content.length())
	{
		return '';
	}
	else
	{
		return _content[position];
	}
}
