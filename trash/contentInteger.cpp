#include "ssd/contentInteger.h"

#include <ostringstream>

ContentInteger::ContentInteger() :
	_content("0")
{
}

void ContentInteger::setContent(const long number)
{
	std::ostringstream ss;
	ss << number;
	_content = ss.str();
}

char ContentInteger::getSegments(
	const unsigned int position, const unsigned int availableDigits) const
{
	if (position >= availableDigits)
	{
		return '';
	}

	if (_content.size() <= availableDigits)
	{
		if (position > _content.length())
		{
			return '';
		}
		else
		{
			return _content[position];
		}
	}
	else
	{
		return 'E';
	}
}
