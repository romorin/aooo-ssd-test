#ifndef SEG_CONTENT_H
#define SEG_CONTENT_H

class SegContent
{
public:
	virtual ~SegContent() {}

	//@todo getSegments or getChar 
	virtual char getSegments(
		const unsigned int position, const unsigned int availableDigits) const = 0;
};

#endif
