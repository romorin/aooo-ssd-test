#include "ssd/contentComposite.h"

#include <forward_list>

ContentComposite::ContentComposite(std::forward_list<Inner> innerContent) :
	_innerContent(innerContent)
{}

ContentComposite::~ContentComposite()
{}

char getSegments(
	const unsigned int position, const unsigned int availableDigits) const
{
	if (position >= availableDigits)
	{
		return '';
	}

	unsigned int pos = 0;
	auto iter = _innerContent.begin();
	auto end = _innerContent.end();

	while (iter != end)
	{
		if (position <= pos + iter->availableDigits)
		{
			unsigned int available = pos + iter->availableDigits <= availableDigits ?
				iter->availableDigits : availableDigits - pos;

			return iter->getSegments(position - pos, available);
		}
		pos += iter->availableDigits;
		iter++;
	}
	return '';
}
